# This script installs Apache on your AWScentOS vm and amends the standard index.html to show the following;
#Hello there.
#My Private IP address is <system IP from ifconfig>
#Your name
#Name and Version of the operating system (obtained from the file /etc/redhat-release or /etc/os-release or /etc/lsb-release)
#The web server software the system is running and the version of the package installed.

HOSTNAME=$1
KEY=~/.ssh/shannon-key-AWS.pem

ssh -o StrictHostKeyChecking=no -i $KEY ec2-user@$HOSTNAME '

# To update repositories 
sudo yum update -y

# To install Apache
sudo yum install -y httpd

sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1>
 Hello There 
</h1>

<h3>
<p> * My Private IP address is 172.31.33.103 </p>
<p> * Shannon Tamplin </p>
<p> * Amazon Linux 2 AMI </p>
<p> * Apache/2.4.52 </p>
</h3>

_END_"

sudo systemctl restart httpd

'
