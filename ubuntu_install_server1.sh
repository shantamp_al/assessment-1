# This script installs nginx onto the ubuntu VM and amends the standard index.html to show the following;
#Hello there.
#My Private IP address is <system IP from ifconfig>
#Your name
#Name and Version of the operating system (obtained from the file /etc/redhat-release or /etc/os-release or /etc/lsb-release)
#The web server software the system is running and the version of the package installed.  

HOSTNAME=$1
KEY=~/.ssh/shannon-key-AWS.pem

if [ -z $HOSTNAME ]
then
    read "you need to add an IP address"
fi

ssh -o StrictHostKeyChecking=no -i $KEY ubuntu@$HOSTNAME '

sudo apt update

if sudo nginx -v ;
then 
    echo "nginx already installed" 
else
    sudo apt install nginx -y 
fi

sudo sh -c "cat >/var/www/html/index.nginx-debian.html <<_END_
<h1>
 Hello There 
</h1>

<h3>
<p> * My Private IP address is 172.31.46.175 </p>
<p> * Shannon Tamplin </p>
<p> * Ubuntu 18.04 </p>
<p> * nginx/1.14.0 (Ubuntu) </p>
</h3>

_END_"

sudo systemctl restart nginx

'


