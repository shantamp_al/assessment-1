# This script installs HA_Proxy onto your ubuntu vm and will amend the config file to add both servers with private IP addresses.

HOSTNAME=$1
KEY=~/.ssh/shannon-key-AWS.pem

if [ -z $HOSTNAME ]
then
    read "you need to add an IP address"
fi

ssh -o StrictHostKeyChecking=no -i $KEY ubuntu@$HOSTNAME '

# To find out what version you can install
sudo apt show haproxy 

# To update repositories 
sudo apt update

# To install HA_proxy
sudo apt install -y haproxy

# To add servers to HA_proxy config
sudo sh -c "cat >/etc/haproxy/haproxy.cfg <<_END_
frontend http_front
   bind *:80
   stats uri /haproxy?stats
   default_backend http_back

backend http_back
   balance roundrobin
   server shannon-ubuntu 172.31.46.175:80 check
   server shannon-aws 172.31.33.103:80 check

_END_"

sudo systemctl restart haproxy

'

open http://$HOSTNAME