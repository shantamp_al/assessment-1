# Assessment 1:

## Virtualisation Marked

### The Task

The task is to create 3 VM in AWS, (Ubuntu Server 18.04 lts x2 and AWS linux - CentOS). Once Created, you wil need to create a bashscript to install nginx on one server and Apache (httpd) on the other server. The last instance will be used as your load balancer(HA_Proxy)

Pre requisite

- AWS account 
- Key Pair to connect to each instance
- Text Editor to write your bashscript. You can use VS Code, Sublime, Vim or Nano

___________________________________________________

### Step 1 : Create a directory to store your code.

You can create your own directory within your local machine and copy my code OR you can use the following command to clone my directory to your local machine using 

```bash 
$ git clone git@bitbucket.org:shantamp_al/assessment-1.git
```

IF you opted to clone my repository and run my script, you will need to make sure the scripts have the required permissions 

```bash
$ chmod +x <bashcriptname>

# To check the file has permissions
$ ll 
> # list of all files in the directory and the bashscript should be highlighted, will also have an 'x' to confirm permissions.
```

### Step 2: Create 3 instances in your AWS account 

<u> First Instance - Server 1 - Ubuntu nginx </u>

- First create your instance and choose your operating system, for this task I have chosen ubuntu 18.04
- Configure Instance Details: Auto-sigin Public IP *Enable*
- Add tag "Name" and put in value relevant name of your server 1
- Create a new seurity group with SSH for your local IP and HTTP for port 80 to allow http to connect from anywhere.
- Preview and launch VM

<u> Second Instance - Server 2 - Apache (httpd) AWS linux 2 AMI </u>

- First create your instance and choose your operating system, for this task I have chosen Amazon linux 2 AMI 
- Configure Instance Details: Auto-sigin Public IP *Enable*
- Add tag "Name" and put in value relevant name of your server 2
- Create a new seurity group with SSH for your local IP and HTTP for port 80 to allow http to connect from anywhere.
- Preview and launch VM

<u> Third Instance - Load Balancer - HA_Proxy (ubuntu)</u>

- First create your instance and choose your operating system, I have chosen the same as my serve 1 ubuntu 18.04
- Configure Instance Details: Auto-sigin Public IP *Enable*
- Add tag "Name" and put in value relevant name of your loadbalancer
- Create a new seurity group with SSH for your local IP and HTTP for port 80 to allow http to connect from anywhere.
- Preview and Launch

### Step 3 - IF you chosen to clone my repository, you will need to amend the bashscripts to your relevant information.

* Lets look at bashscript **ubuntu_install_server1.sh**

On line '9' you will see that I have created a variable for my key pair, you will need to change this to the correct path you have saved your key pair. HINT: This is usually stored in your .ssh folder in your local machine.



